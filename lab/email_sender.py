# -*- coding: utf-8 -*-
import smtplib
import email.utils
from email.mime.text import MIMEText


def create_and_send_email():
    """Funkcja interaktywna pytająca użytkownika o potrzebne dane
    i wysyłająca na ich podstawie list elektroniczny.
    """
    # Odczyt danych od użytkownika
    nadawca = raw_input("podaj skad wysyalsz")
    adresat = raw_input("podaj adres adresata")
    temat = raw_input("podaj temat")
    tresc = raw_input("podaj tresc")
    # Stworzenie wiadomości
    wiadomsoc = MIMEText(tresc, _charset='utf-8')
    wiadomsoc['From'] = email.utils.formataddr(("Nadawca", nadawca))
    wiadomsoc['To'] = email.utils.formataddr(("Adresat", adresat))
    wiadomsoc['Subject'] = temat
    msg = wiadomsoc.as_string()

    server = smtplib.SMTP('194.29.175.240', 25)
    try:
        # Połączenie z serwerem pocztowym

        # Ustawienie parametrów
        server.set_debuglevel(True)
        server.ehlo()


        # Autentykacja
        server.starttls()
        server.ehlo()
        server.login("p13","p13")
        # Wysłanie wiadomości
        server.sendmail(nadawca, [adresat, ], msg)
    except Exception as e:
        print str(e)

    finally:
        # Zamknięcie połączenia
        server.close()


if __name__ == '__main__':
    decision = 't'
    while decision in ['t', 'T', 'y', 'Y']:
        create_and_send_email()
        decision = raw_input('Wysłać jeszcze jeden list? ')
