# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config
from email.utils import formatdate
import datetime
import time


def handle_client(connection, html, logger):
    """Obsługa konwersacji HTTP z pojedynczym klientem

    connection: socket klienta
    html:       wczytana strona html do zwrócenia klientowi
    logger:     mechanizm do logowania wiadomości
    """
    # Odebranie żądania
    buffer = 2048
    czyWykonane = False
    dane = ''
    while not czyWykonane:
        request = connection.recv(1024)
        if len(request) < buffer:
            czyWykonane = True
        dane += request


    # TODO: poprawnie obsłużyć żądanie dowolnego rozmiaru

    logger.info(u'odebrano: "{0}"'.format(request))

    # Wysłanie zawartości strony
    if ((dane.find("GET")!=-1)and dane.find("HTTP") !=-1):
        # Wysłanie zawartości strony
        data = datetime.datetime.now()
        tuple = data.timetuple()
        czas = time.mktime(tuple)
        dane = formatdate(czas)

        naglowek = "HTTP/1.1 200 OK \r\nContent-type: text/html \r\nDate: %s \r\nContent-Length: %s \r\n\r\n"% (dane, len(html))
        connection.sendall(naglowek )
        connection.sendall(html)  # TODO: czy to wszystko?
    else:
        naglowek  = "HTTP/1.1 405 Method Not Allowed \r\n"
        connection.sendall(naglowek )
    logger.info(u'wysyłano odpowiedź')


def http_serve(server_socket, html, logger):
    """Obsługa połączeń HTTP

    server_socket:  socket serwera
    html:           wczytana strona html do zwrócenia klientowi
    logger:         mechanizm do logowania wiadomości
    """
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        logger.info(u'połączono z {0}:{1}'.format(*client_address))

        try:
            handle_client(connection, html, logger)

        finally:
            # Zamknięcie połączenia
            connection.close()


def server(logger):
    """Server HTTP

    logger: mechanizm do logowania wiadomości
    """
    # Tworzenie gniazda TCP/IP
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Ustawienie ponownego użycia tego samego gniazda
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem
    server_address = ('194.29.175.240', 8888)  # TODO: zmienić port!
    server_socket.bind(server_address)
    logger.info(u'uruchamiam server na {0}:{1}'.format(*server_address))

    # Nasłuchiwanie przychodzących połączeń
    server_socket.listen(1)

    html = open('web/web_page.html').read()

    try:
        http_serve(server_socket, html, logger)

    finally:
        server_socket.close()


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('http_server')
    server(logger)
    sys.exit(0)