
import socket
import sys
import logging
import logging.config
import os
import httplib

import email.utils
import time

def handle_client(connection, html):

    print('Jestem w metodzie handle_client')
    odebrane = connection.recv(1024)
    try:
        if odebrane:
            link = odebrane.split('\r\n')
            tmp = link[0].split()
            URI = tmp[1]
            URI = 'web' + URI
            print(URI)
            html = ''
            head = ''
            if URI == 'web/':
                html = open('web/web_page.html').read()
                head = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8 \r\nContent-Length: %s \r\n\r\n'% (len(html))
                print("Wyslanie zawartosci strony web_page.html")
                connection.sendall(head+html)
            else:
                if URI[-3:] == 'txt':
                    try:
                        html += open(URI,"rb").read()
                        head = 'HTTP/1.1 200 OK\r\nContent-Type: text/plain; charset=UTF-8 \r\nContent-Length: %s \r\n\r\n'% (len(html))
                        #connection.sendall(strona(html, head))
                    except:
                        head = 'HTTP/1.1 400 Not Found\r\nContent-Type: text/plain; charset=UTF-8\r\n\r\n'
                        html = '400 Not Found'
                        #connection.sendall(strona(html, head))
                elif URI[-3:] == 'jpg':
                    try:
                        html = '<a href=/"'
                        html += open(URI,"rb").read()
                        html += '">Zdjecie</a>'
                        head = 'HTTP/1.1 200 OK\r\nContent-Type: text/jpg; charset=UTF-8 \r\nContent-Length: %s \r\n\r\n'% (len(html))
                        #connection.sendall(strona(html, head))
                    except:
                        head = 'HTTP/1.1 400 Not Found\r\nContent-Type: text/jpg; charset=UTF-8\r\n\r\n'
                        html = '400 Not Found'
                        #connection.sendall(strona(html, head))
                elif URI[-3:] == 'png':
                    try:
                        html = '<a href="/'
                        html += open(URI,"rb").read()
                        html += '">Zdjecie</a>'
                        head = 'HTTP/1.1 200 OK\r\nContent-Type: text/png; charset=UTF-8 \r\nContent-Length: %s \r\n\r\n'% (len(html))
                        #connection.sendall(strona(html, head))
                    except:
                        head = 'HTTP/1.1 400 Not Found\r\nContent-Type: text/png; charset=UTF-8\r\n\r\n'
                        html = '400 Not Found'
                        #connection.sendall(strona(html, head))
            if os.listdir(URI):
                try:
                    for plik in os.listdir(URI):
                        html = html = '<a href="/'
                        html += plik + '</a>'
                        head = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8 \r\nContent-Length: %s \r\n\r\n'% (len(html))
                        #connection.sendall(strona(html, head))
                except:
                    head = 'HTTP/1.1 400 Not Found\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n'
                    html = '400 Not Found'
                    #connection.sendall(strona(html, head))
    except:
        head = 'HTTP/1.1 400 Not Found\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n'
        html = '500 Internal Server Error'
        connection.sendall(strona(html, head))

    print("Zamkniecie polaczenia")
    connection.close()

def strona(tresc, inf):
    html = inf + '<!DOCTYPE html><head></head><body><div>'
    html += tresc
    html += '</div></body></html>'
    return html


def http_server(server_socket, html):

    while True:
        print('Czekam na polaczenie z serwerem......')
        connection, client_address = server_socket.accept()

        try:
            handle_client(connection, html)
        finally:
            connection.close()

def server():
    print('W metodzie server')
    #Tworzenie gniazda tcp/ip
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #Ustawianie ponownego uzycia teg samego gniazda
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server_address = ('localhost', 58213)

    server_socket.bind(server_address)

    server_socket.listen(1)

    html = open('web/web_page.html').read()

    try:
        http_server(server_socket, html)

    finally:
        server_socket.close()


def checkForResponse(host,port,request,page='/'):
    try:
        conn = httplib.HTTPConnection(host,port)
        conn.request(request,page)
        response = conn.getresponse()
    except:
        return '',''

    conn.close()
    return response.status, response.getheaders()


def lookforobject(headers, object):

    isPresent = False

    for item in headers:

        if ('content-type' in item) and (object in item[1]):
            isPresent = True

    return isPresent

if __name__ == '__main__':

    server()

