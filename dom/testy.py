__author__ = 'bartek'

import unittest

from http_server import checkForResponse,lookforobject

class Testy(unittest.TestCase):


    def testForResponseandContent1(self):
        response = checkForResponse('localhost',58213,'GET')
        ispresent = lookforobject(response[1],'text/html')
        assumed = '',False
        self.assertEqual(response[0],assumed[0])
        self.assertEqual(ispresent,assumed[1])
    def testForResponseandContent2(self):
        response = checkForResponse('localhost',58213,'GET','/web_page.html')
        ispresent = lookforobject(response[1],'text/html')
        assumed = '',False
        self.assertEqual(response[0],assumed[0])
        self.assertEqual(ispresent,assumed[1])
    def testForResponseandContent3(self):
        response = checkForResponse('localhost',58213,'GET','/lokomotywa.txt')
        ispresent = lookforobject(response[1],'text/html')
        assumed = '',False
        self.assertEqual(response[0],assumed[0])
        self.assertEqual(ispresent,assumed[1])
    def testForResponseandContent4(self):
        response = checkForResponse('localhost',58213,'GET','/images/gnu_meditate_levitate.png')
        ispresent = lookforobject(response[1],'image/jpeg')
        assumed = '',False
        self.assertEqual(response[0],assumed[0])
        self.assertEqual(ispresent,assumed[1])
    def testForResponseandContent6(self):
        response = checkForResponse('localhost',58213,'GET', '/images/jpg_rip.jpg')
        ispresent = lookforobject(response[1],'image/jpeg')
        assumed = '',False
        self.assertEqual(response[0],assumed[0])
        self.assertEqual(ispresent,assumed[1])


if __name__ == '__main__':
    unittest.main()